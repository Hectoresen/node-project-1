const express = require('express');
const PORT = 3000;
const server = express();
const router = express.Router();
const {connect} = require('./config/db');
const TeamsModel = require('./models/teams');
connect();

router.get('/', (req, res) =>{
    return res.send('<h1>Welcome to teams finder</h1>')
})

//Get allteams
router.get('/allteams', async (req, res) =>{
    try {
        const teams = await TeamsModel.find();
        return res.status(200).json(teams);
    } catch(err){
        return res.status(404).json(err);
    }
});
//Search by id
router.get('/allteams/:id', async(req, res) =>{
    try{
        const id = req.params.id;
        const teams = await TeamsModel.findById(id);
            return res.status(200).json(teams);
    }catch(err){
        return res.status(404).json(err)
    }
});
//Team search
router.get('/allteams/name/:team', async (req, res) =>{
    try{
        const name = req.params.team;
        const teams = await TeamsModel.find({
            team: {$regex: new RegExp("^" + name.toLowerCase(), "i"),
        },
        });
        if(!teams.length){
            return res.status(404).json('We have not found the requested content')
        }
        return res.status(200).json(teams);
    }catch(err){
        return res.status(404).json(err);
    }
});
//Search by position league
router.get('/allteams/name/team/:leagueposition', async (req, res) =>{
    try {
        const leaguePositionTeam = req.params.leagueposition;
        const teams = await TeamsModel.find({leaguePosition: leaguePositionTeam});
        if(!teams.length){
            return res.status(404).json('We have not found the requested content');
        }
        return res.status(200).json(teams);
    } catch(err){
        return res.status(404).json(err);
    }
});
//Search same goals or greater
router.get('/allteams/name/team/goals/:totalgoals', async (req, res) =>{
    try {
        const teamGoals = req.params.totalgoals;
        const teams = await TeamsModel.find({totalGoals:{$gte:teamGoals}})
        if(!teams.length){
            return res.status(404).json('We have not found the requested content');
        }
        return res.status(200).json(teams);
    } catch (err){
        return res.status(404).json(err);
    }
});

server.use('/', router);

server.listen(PORT, () =>{
    console.log(`Server running in http://localhost:${PORT}`);
});