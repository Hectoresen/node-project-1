const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const teamSchema = new Schema({
    team: {type: String, required: true},
    leaguePosition: {type: Number, required: true},
    totalGoals: {type: Number, required: false},
},
{timestamps: true}
);

const TeamsModel = mongoose.model('team', teamSchema);

module.exports = TeamsModel;