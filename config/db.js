const mongoose = require('mongoose');
const DB_URL = 'mongodb://localhost:27017/football-project-1';
const CONFIG_DB = {useNewUrlParser: true, useUnifiedTopology: true};

const connect = async () =>{
    try{
        const res = await mongoose.connect(DB_URL, CONFIG_DB);
        const {port, name} = res.connection;
        console.log(`Connected to ${name} on port ${port}`);
    } catch(err){
        console.log('Database connection failed');
    }
}

module.exports = {
    DB_URL,
    CONFIG_DB,
    connect,
};